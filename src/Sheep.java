
public class Sheep {

	enum Animal {
		sheep, goat
	};

	public static void main(String[] param) {
		// for debugging
	}

	public static void reorderFirst(Animal[] animals) {

		int goatCounter = 0;

		// Kui midagi pole sorteerida, lõpetan kohe tegevuse.

		if (animals.length < 2) {
			return;
		}

		// Kõigepealt sorteerin sikud massiivi algusesse

		for (int i = 0; i < animals.length; i++) {

			// Loendan kõik massiivis olevad kitsed kokku...

			if (animals[i] == Animal.goat) {
				goatCounter = goatCounter + 1;
			}

			// ...ja määran hetkeks kõik massiivi liikmed sikkudeks.

			animals[i] = Animal.goat;
		}

		// Piirjuhtum, kui kõik massiivi liikmed olid sikud.

		if (goatCounter == animals.length) {
			return;
		} else {

			// For-tsükkel algab massiivi liikmest, pärast mida ei tohi enam
			// ühtegi sikku olla (goatCounter)

			for (int i = goatCounter; i < animals.length; i++) {

				// Pärast goatCounter'ndat liiget on kõik liikmed lambad.

				animals[i] = Animal.sheep;
			}
		}

	}

	// Lisanduv meetod tühja massiivi abiga.

	public static void reorder(Animal[] animals) {

		// Kui midagi pole sorteerida, lõpetan kohe tegevuse.

		if (animals.length < 2) {
			return;
		}

		// Loon uue sama suure massiivi, kuhu hakata sorteeritud sikke-lambaid
		// panema.
		Animal[] orderedAnimals = new Animal[animals.length];

		// Massiivi järg algusest, nn sikuloendaja
		int orderedAnimalsStart = 0;

		// Massiivi järg lõpust, nn lambaloendaja
		int orderedAnimalsEnd = animals.length - 1;

		for (int i = 0; i < animals.length; i++) {

			// Kui tegu on sikuga, paneme uude massiivi kohale [sikuloendaja]
			// siku ning suurendame sikuloendurit ühe võrra.
			if (animals[i] == Animal.goat) {
				orderedAnimals[orderedAnimalsStart] = Animal.goat;
				orderedAnimalsStart = orderedAnimalsStart + 1;
			}

			// Aga kui tegu on lambaga, paneme uude massiivi kohale
			// [lambaloendaja] lamba ning vähenedame lambaloendurit ühe võrra.
			else if (animals[i] == Animal.sheep) {
				orderedAnimals[orderedAnimalsEnd] = Animal.sheep;
				orderedAnimalsEnd = orderedAnimalsEnd - 1;
			}
		}

		// Kui algne massiiv on läbi töötatud, asendame algse massiivi reastatud
		// massiivi koopiaga.

		System.arraycopy(orderedAnimals, 0, animals, 0, orderedAnimals.length);

	}
}
